#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/threading/spin_wait.hpp>

namespace solutions {

/*  QueueSpinLock spinlock;
 *
 *  {
 *    QueueSpinLock::Guard guard(spinlock);  // Acquires spinlock
 *    // Critical section starts here
 *  }  // Releases spinlock
 */

class QueueSpinLock {
 public:
  class Guard {
   public:
    explicit Guard(QueueSpinLock& spinlock) : spinlock_(spinlock) {
      // Your code goes here
    }

    ~Guard() {
      // Your code goes here
    }

   private:
    QueueSpinLock& spinlock_;
    // Your code goes here
  };

 private:
  // Your code goes here
};

}  // namespace solutions
