# Getting Started

## Environment

Для работы с репозиторием вам необходимо настроить окружение. 

Все необходимые зависимости описаны [здесь](https://gitlab.com/Lipovsky/shad-tpcc-course-2019/tree/master/docker).

Если вы умеете пользоваться Докером, то попробуйте работать сразу в нем.

### Ограничения

- Мы поддерживаем *только* 64-битный Linux.
- Компилятор – *только* clang++ версии не меньше 6.0.

Эти ограничения нужны для того, чтобы обеспечить предсказуемое поведение сборки и тестов на ваших локальных машинах.

## Докер

Если вы не хотите устанавливать все необходимые пакеты и компиляторы вручную, то можете развернуть ```docker```-контейнер, в котором будет все необходимое. Инструкции написаны [здесь](https://gitlab.com/Lipovsky/shad-tpcc-course-2019/tree/master/docs/docker.md). По завершении установки переходите ко второму пункту следующей части данного руководства.

## Репозиторий и клиент

1. Заведите директорию для работы с курсом

```
mkdir tpcc
cd tpcc
```

В этой директории будет жить репозиторий курса и репозиторий с вашими решениями. 

2. Клонируйте в эту директорию репозиторий курса:

```
git clone https://gitlab.com/Lipovsky/shad-tpcc-course-2019.git
```

3. Перейдите в корень рабочей копии и запустите

```
./init.sh
```

Команда `./init.sh`
1) Подтянет необходимые сабмодули и
2) Запустит установку консольного клиента, с помощью которого вы будете работать с задачами.

## Установка клиента

Внимательно и осознанно отвечайте на все вопросы, которые задает вам скрипт установки.

После завершения установки скрипт запустит клиент с командой `help`.

После успешного завершения скрипта установки перезапустите терминал.

## Клиент

После установки выполните `tpcc cmake`, чтобы сгенерировать файлы для сборки.

Теперь вы можете выбрать любую задачу, написать решение и протестировать его командой `tpcc test`.

У клиента есть и другие команды. Изучите их, написав в терминале `tpcc help`

## Troubleshooting

Возможно вам пригодятся ссылки из [Toolchain](https://gitlab.com/Lipovsky/shad-tpcc-course-2019/blob/master/docs/toolchain.md)

