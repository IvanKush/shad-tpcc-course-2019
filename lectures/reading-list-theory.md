# Multithreading

## Fine-grained locking

- [A Lazy Concurrent List-Based Set Algorithm](https://people.csail.mit.edu/shanir/publications/Lazy_Concurrent.pdf)
- [A Simple Optimistic skip-list Algorithm](http://people.csail.mit.edu/shanir/publications/LazySkipList.pdf)
- [Algorithmic Improvements for Fast Concurrent Cuckoo Hashing](https://www.cs.princeton.edu/~mfreed/docs/cuckoo-eurosys14.pdf), [libcuckoo](https://github.com/efficient/libcuckoo)

## Memory consistency models

- [Memory Barriers: a Hardware View for Software Hackers](http://www.puppetmastertrading.com/images/hwViewForSwHackers.pdf)
- [Memory Models: A Case For Rethinking Parallel Languages and Hardware](https://cacm.acm.org/magazines/2010/8/96610-memory-models-a-case-for-rethinking-parallel-languages-and-hardware/pdf)
- [Explanation of the Linux-Kernel Memory Consistency Model](https://github.com/torvalds/linux/tree/master/tools/memory-model/Documentation) 
- [Java Memory Model](https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html#jls-17.4)
- [Foundations of the C++ Concurrency Memory Model](http://www.hpl.hp.com/techreports/2008/HPL-2008-56.pdf)
- [Weak Memory Consistency](https://people.mpi-sws.org/~viktor/wmc/)
- [Weakly Consistent Concurrency](https://www.cs.tau.ac.il/~orilahav/seminar18/index.html)

## Lock-Free data structures

- [Simple, Fast, and Practical Non-Blocking and Blocking Concurrent Queue Algorithms](http://www.cs.rochester.edu/~scott/papers/1996_PODC_queues.pdf)
- [Split-Ordered Lists: Lock-Free Extensible Hash Tables](http://people.csail.mit.edu/shanir/publications/Split-Ordered_Lists.pdf)
- [Cliff Click – A Lock-Free Hash Table](https://www.youtube.com/watch?v=HJ-719EGIts)
- [Цикл статей про lock-free структуры данных](https://habr.com/post/195770/)
- [Data Structures in the Multicore Age](http://people.csail.mit.edu/shanir/publications/p76-shavit.pdf) 

## Lock-Freedom и Wait-Freedom

- [A Practical Multi-Word Compare-and-Swap Operation](https://www.cl.cam.ac.uk/research/srg/netos/papers/2002-casn.pdf)
- [Wait-Free Synchronization](https://cs.brown.edu/~mph/Herlihy91/p124-herlihy.pdf)
- [On the Nature of Progress](http://citeseerx.ist.psu.edu/viewdoc/download;?doi=10.1.1.469.3698&rep=rep1&type=pdf)
- [Are Lock-Free Concurrent Algorithms Practically Wait-Free?](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/paper-18.pdf)

## Consistency models for concurrent objects

- [Linearizability: A Correctness Condition for Concurrent Objects ](https://cs.brown.edu/~mph/HerlihyW90/p463-herlihy.pdf)
- [Consistency Models Map](https://jepsen.io/consistency)
- [Linearizability versus Serializability](http://www.bailis.org/blog/linearizability-versus-serializability/)
- [Strong consistency models](https://aphyr.com/posts/313-strong-consistency-models)

## Transactions
 
- [ Martin Kleppmann – Transactions: myths, surprises and opportunities](https://www.youtube.com/watch?v=5ZjhNTM8XU8), [слайды и ссылки](https://martin.kleppmann.com/2015/09/26/transactions-at-strange-loop.html)
- [A Critique of ANSI SQL Isolation Levels](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-95-51.pdf)
- [Serializable Isolation for Snapshot Databases](https://ses.library.usyd.edu.au/bitstream/2123/5353/1/michael-cahill-2009-thesis.pdf)
- [What Write Skew Looks Like](https://www.cockroachlabs.com/blog/what-write-skew-looks-like/)
- [Serializable Snapshot Isolation in PostgreSQL](https://drkp.net/papers/ssi-vldb12.pdf)

## Hardware Transactional Memory

- [Maurice Herlihy – Transactional Memory](https://www.youtube.com/watch?v=ZkUrl8BZHjk), [слайды](http://neerc.ifmo.ru/sptcc/slides/slides-herlihy.pdf)
- [Gil Tene – Understanding Hardware Transactional Memory](https://www.youtube.com/watch?v=0jy4Sc_IY7o)
- [Глава 16 – Programming with Intel Transactional Synchronization Extensions](https://software.intel.com/sites/default/files/managed/39/c5/325462-sdm-vol-1-2abcd-3abcd.pdf)
- [Глава 13 – Intel TSX Recommendations](https://software.intel.com/sites/default/files/managed/9e/bc/64-ia-32-architectures-optimization-manual.pdf)

# Distributed systems

## Time

- [Time, Clocks and GPS](http://www2.unb.ca/gge/Resources/gpsworld.nov-dec91.corr.pdf)
- [Relativistic Effects in the Global Positioning System](https://www.aapt.org/doorway/TGRU/articles/Ashbyarticle.pdf)

## Replicated R/W Register

- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 16

## Replication, Atomic Broadcast and Consensus

- [Unreliable Failure Detectors for Reliable Distributed Systems](https://www.cs.utexas.edu/~lorenzo/corsi/cs380d/papers/p225-chandra.pdf)
- [Sequential Consistency versus Linearizability](https://pdfs.semanticscholar.org/787c/d132b7c849863393a01ae8f0d92b6a3f8cb3.pdf)

## Impossibility of Consensus

- [Impossibility of Distributed Consensus with One Faulty Process](https://groups.csail.mit.edu/tds/papers/Lynch/jacm85.pdf)
- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 11
- [On the Minimal Synchronism Needed for Distributed Consensus](https://www.cs.huji.ac.il/~dolev/pubs/p77-dolev.pdf)

## Single Decree Paxos

- [Part-Time Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf)
- [Paxos Made Simple](https://lamport.azurewebsites.net/pubs/paxos-simple.pdf)
- Заметки Лесли Лампорта про [Part-Time Parliament](http://lamport.azurewebsites.net/pubs/pubs.html#lamport-paxos) и [Paxos Made Simple](http://lamport.azurewebsites.net/pubs/pubs.html#paxos-simple)
- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 12

## Formal Methods, TLA+

- [TLA+ Home Page](https://lamport.azurewebsites.net/tla/tla.html)
- [TLA+ Video Course](http://lamport.azurewebsites.net/video/videos.html)
- [If You’re Not Writing a Program, Don’t Use a Programming Language](http://bulletin.eatcs.org/index.php/beatcs/article/view/539/532)
- [What Good is Temporal Logic?](https://www.microsoft.com/en-us/research/uploads/prod/2016/12/What-Good-Is-Temporal-Logic.pdf)
- [TLA+ In Practice and Theory](https://pron.github.io/posts/tlaplus_part1)
- [Learn TLA+](https://learntla.com/introduction/)
- [Dr. TLA+ Series](https://github.com/tlaplus/DrTLAPlus)